import {
  Input,
  Box,
  Button,
  Container,
  VStack,
  HStack,
} from "@chakra-ui/react";
import Message from "./components/Message";
import {
  signOut,
  onAuthStateChanged,
  getAuth,
  GoogleAuthProvider,
  signInWithPopup,
} from "firebase/auth";
import { app } from "./components/firebase";
import { useEffect, useRef, useState } from "react";

import {
  query,
  orderBy,
  onSnapshot,
  addDoc,
  collection,
  getFirestore,
  serverTimestamp,
} from "firebase/firestore";
//import { async } from "@firebase/util";
const auth = getAuth(app);
const db = getFirestore(app);

const loginhandler = () => {
  const provider = new GoogleAuthProvider();

  signInWithPopup(auth, provider);
};

const logouthandler = () => {
  signOut(auth);
};

function App() {
 
  const [user, setUser] = useState(false);
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);
  const divForScroll = useRef(null);

  const submithendler = async (e) => {
    e.preventDefault();

    try {
      setMessage("");

      await addDoc(collection(db, "Messages"), {
        text: message,
        uid: user.uid,
        uri: user.photoURL,
        createdAt: serverTimestamp(),
      });

      divForScroll.current.scrollIntoView({ behavior: "smooth" });
    } catch (error) {
      alert(error);
    }
  };

  useEffect(() => {
    const q = query(collection(db, "Messages"), orderBy("createdAt", "asc"));
    const unsubcribe = onAuthStateChanged(auth, (data) => {
      setUser(data);
    });
    const unsubscribeForMessage = onSnapshot(q, (snap) => {
      setMessages(
        snap.docs.map((item) => {
          const id = item.id;
          return { id, ...item.data() };
        })
      );
    });

    return () => {
      unsubcribe();
      unsubscribeForMessage();
    };
  },[]);
  return (
    <Box bg={"red.50"}>
      {user ? (
        <Container height={"100vh"} bg={"white"}>
          <VStack h="fill" paddingY={"4"}>
            <Button colorScheme={"red"} w={"full"} onClick={logouthandler}>
              Logout
            </Button>
          </VStack>
          <VStack h="full" w={"full"} overflowY={"auto"} css={{"&::-webkit-scrollbar":{
            display:"none",
          }}}>
            {messages.map((item) => (
              <Message
                key={item.id}
                text={item.text}
                user={item.uid === user.uid ? "me" : "other"}
                uri={item.uri}
              />
            ))}
            <div ref={divForScroll}></div>
          </VStack>

          <form
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            style={{ width: "100%" }}
            onSubmit={submithendler}
          >
            <HStack>
              <Input placeContent={"Enter a Maessage"} />
              <Button colorScheme={"purple"} type="submit">
                Send
              </Button>
            </HStack>
          </form>
        </Container>
      ) : (
        <VStack justifyContent={"center"} h={"100vh"}>
          <Button colorScheme={"purple"} onClick={loginhandler}>
            
            Sign in with Google
          </Button>
        </VStack>
      )}
    </Box>
  );
}

export default App;
